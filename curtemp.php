<?php

/*

  curtemp.php
  By Jeffrey Sung
  Webpage that displays most recent temperature measurement

*/

require_once('../../../includes/monconnection.php');

$host=$hostname_pub_connect;
$user=$username_pub_connect;
$pass=$password_pub_connect;
$db=$database_pub_connect;

$dbh=mysql_connect($host,$user,$pass) or die("Unable to connect<br/>");
mysql_select_db($db);
$query="SELECT * FROM temperature WHERE 1 ORDER BY date DESC LIMIT 1";
$data=mysql_query($query);
//precondition: limit 1 in query
$result=mysql_fetch_assoc($data);
$temperature=$result['temperature'];
$timestamp=$result['date'];

mysql_close($dbh);



?>
<html>
<head>
<title>Metropolis1 Temperature Monitor</title>
</head>
<body>
<p><h1><div align="center">Temperature Monitor</div></h1></p>
<p><h1><div align="center"><?php echo $temperature; ?> &deg;C</div></h1></p>
<p><div align="center">As of <?php echo $timestamp; ?> </div></p>
<p><div align="center">&copy; 2015 - <?php echo date("Y"); ?></p>
</body>
</html>
